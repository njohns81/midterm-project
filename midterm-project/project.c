//main project program.
//Partners: Noah Johnson(njohns81) and Anum Haque(ahaque9)

#include "ppmIO.h"
#include "commandUtil.h"
#include "imageManip.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[]) {
  input(argc, argv);
  return 0;
}
