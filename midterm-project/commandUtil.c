//Partners: Noah Johnson(njohns81) and Anum Haque(ahaque9)
#include "commandUtil.h"
#include <stdio.h>
#include <string.h>
#include "ppmIO.h"
#include "imageManip.h"
#include <stdlib.h>
//contains implementations of all the functions related to reading user input from command line

int input(int argc, char* argv[]) {
  char user_input[30];
  FILE* in = NULL;
  FILE* out = NULL;

  //check to see if user did not supply input or output filename
  if(argc == 1) {
    printf("Failed to supply input filename or output filename");
    return 1;
  }
  //open file for writing
  out = fopen(argv[2], "wb");
  if(out == NULL) {
    printf("Specified output file could not be opened for writing");
    return 4;
  }

  //open file for reading
  in = fopen(argv[1], "rb");
  if(in == NULL) {
    printf("Specified input file could not be found");
    return 2;
  }

  //copy user input into argv[3]
  strcpy(user_input, argv[3]);  
  Image* img = readPPM(in);


  //swap image
  if(strcmp(user_input, "swap") == 0) {
    writePPM(out, swap(img));
   
    
  }

  //grayscale image
  else if(strcmp(user_input, "grayscale") == 0) {
    writePPM(out, grayscale(img));
   
    
  }

  //apply blackout to image
  else if(strcmp(user_input, "blackout") == 0) {
    writePPM(out, blackout(img));
  
   
  }

  //contrast image
  else if(strcmp(user_input, "contrast") == 0) {
    double c = atof(argv[4]);
    writePPM(out, contrast(img, c));
   
  }
  //crop image
  else if(strcmp(user_input, "crop") == 0) {
  
    int r1 = atoi(argv[4]);
    int c1 = atoi(argv[5]);
    int r2 = atoi(argv[6]);
    int c2 = atoi(argv[7]);
    
   
    if(argv[8] != NULL) {
      printf("Wrong number of arguments");
      return 6;
    }

    writePPM(out, (crop(img, r1, c1, r2, c2)));
  }
  return 0;
  fclose(out);
  fclose(in);
  free(img);
  free((*img).data);
}
    

