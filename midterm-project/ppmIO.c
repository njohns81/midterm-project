//ppmIO.c
//601.220, Spring 2018
//Starter code for midterm project - feel free to edit/add to this file
//Partners: Noah Johnson(njohns81) and Anum Haque(ahaque9)
#include <stdlib.h>
#include <stdio.h>
#include "ppmIO.h"
#include "imageManip.h"
#include "commandUtil.h"


/* read PPM formatted image from a file (assumes fp != NULL) */
Image* readPPM(FILE *fp) {

  char f_spec[1000];
  char ch;
  int rgb_comp = 0;

  //checks to see if the header starts with P6, if not, print an error message
  fgets(f_spec, sizeof(f_spec), fp);
  if(f_spec[0] != 'P' && f_spec[1] != '6') {
    printf("Specified input file is not a properly formatted PPM file");
    return NULL;
  }
  fscanf(fp, "%c", &ch);
  if(ch == '#') {
    ungetc(ch, fp);
    fgets(f_spec, sizeof(f_spec), fp);
  }
  else {
    ungetc(ch, fp);
  }

   int num_rows;
   int num_cols;
   Image *img;
   img = (Image *) malloc(sizeof(Image));
   if(!img) {
     fprintf(stderr, "unable to allocate memory\n");
   }
    
   //read in the dimensions of the picture and make an array of the same size 
   fscanf(fp, "%d", &num_cols);
   fscanf(fp, "%d", &num_rows);
   (*img).data = (Pixel *)malloc(num_rows * num_cols * sizeof(Pixel));
   (*img).cols = num_cols;
   (*img).rows = num_rows;
   

   //read the next int value in the header
   fscanf(fp, "%d", &rgb_comp);
   //check if the color value is 255
   if(rgb_comp != 255 ){
     fprintf(stderr, "invalid rgb component\n");
     //return 8;
   }
   //read in the entire image as an array
   fgetc(fp);
   fread((*img).data, sizeof(Pixel), (*img).rows * (*img).cols, fp);
   

   fclose(fp);
   return img; 
}




/* write PPM formatted image to a file (assumes fp != NULL and img != NULL) */
int writePPM(FILE *fp, const Image* im) {

  /* abort if either file pointer is dead or image pointer is null; indicate failure with -1 */
  if (!fp || !im) {
    printf("Writing output failed");
    return -1;
  }

  /* write tag and dimensions; colors is always 255 */
  fprintf(fp, "P6\n%d %d\n%d\n", im->cols, im->rows, 255);

  /* write pixels */
  int num_pixels_written = (int) fwrite(im->data, sizeof(Pixel), (im->rows) * (im->cols), fp);

  /* check if write was successful or not; indicate failure with -1 */
  if (num_pixels_written != (im->rows) * (im->cols)) {
    printf("Writing output failed");
    return -1;
  }

  /* success, so return number of pixels written */
  return num_pixels_written;
}



