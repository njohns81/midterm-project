//Partners: Noah Johnson(njohns81) and Anum Haque(ahaque9)
#include "imageManip.h"
#include "commandUtil.h"
#include "ppmIO.h"
#include <stdio.h>
#include <stdlib.h>
//contains implementations of all image manipulation algorithms


Image* swap(Image* img) {
  int rows = (*img).rows;
  int cols = (*img).cols;
  Pixel* data = (*img).data;

  //swap rgb components of image
  for(int r = 0; r < rows; r++) {
    for(int c = 0; c < cols; c++) {
      unsigned char temp = data[cols*r+c].r;
      data[cols*r+c].r = data[cols*r+c].g;
      data[cols*r+c].g = data[cols*r+c].b;
      data[cols*r+c].b = temp;
    }
  }
     
  return img;
}

Image* blackout(Image* img) {
  int rows = (*img).rows;
  int cols = (*img).cols;
  Pixel* data = img->data;
  //blackout top right corner if number of columns and rows is even
  if(rows % 2 == 0 && cols % 2 == 0) {
    int i, j;
    for(i = 0; i < rows/2; i++) {
      for(j = cols/2; j < cols; j++) {
	 data[cols*i+j].r = 0;
	 data[cols*i+j].g = 0;
	 data[cols*i+j].b = 0;
      }
    }
  }
  //blackout top right corner if number of rows is odd and number of columns is even
  else if(rows % 2 != 0 && cols % 2 == 0) {
    for(int r = 0; r <= (rows/2)-1; r++) {
      for(int c = (cols)/2; c < cols; c++) {
	 data[cols*r+c].r = 0;
	 data[cols*r+c].g = 0;
	 data[cols*r+c].b = 0;
      }
    }
  }

  //blackout top right corner if number of columns is odd and number of rows is even
  else if(rows % 2 == 0 && cols %2 != 0) {
    for(int p = 0; p < rows / 2; p++) {
      for(int q = (cols / 2) + 1; q < cols; q++) {
	data[cols*p+q].r = 0;
	data[cols*p+q].g = 0;
	data[cols*p+q].b = 0;
      }
    }
  }
  else {
    for(int m = 0; m <= (rows/2)-1; m++) {
      for(int n = (cols/2)+1; n < cols; n++) {
	data[cols*m+n].r = 0;
	data[cols*m+n].g = 0;
	data[cols*m+n].b = 0;
      }
    }
  }
  return img;
}

Image* crop(Image* img, int x1, int y1, int x2, int y2) {
  Image* cropped = malloc(sizeof(Image));
  Pixel* data  = img-> data;
  int rows = (*img).rows;
  int cols = (*img).cols;
  int new_row = y2-y1;
  int new_col = x2-x1;
  cropped->rows = new_row;
  cropped->cols = new_col;
  if(y2 > rows && y1 < 0) {
    printf("arguments for crop operation were out of range for the given input image");
  }

  if(x1 < 0 && x2 > cols) {
    printf("arguments for crop operation were out of range for the given input image");
  }

  //crop image
  (*cropped).data = (Pixel*)malloc((new_row)*(new_col)*sizeof(Pixel));
  for(int i = 0; i < new_row; i++) {
   for(int j = 0; j < new_col; j++) {
     (*cropped).data[i * new_row +j] = data[(y1 + i) * rows + (x1 +j)];
   }
  }
  if(cropped == NULL) {
    printf("null");
  }
  
  return cropped;
}

Image* grayscale(Image* img) {
  int rows = (*img).rows;
  int cols = (*img).cols;
  Pixel* data = img->data;
  //make image gray
  for(int ro = 0; ro < rows; ro++) {
    for(int co = 0; co < cols; co++) {
      double grey = ((.3*data[cols*ro+co].r) + (0.59*data[cols*ro+co].g) + (.11*data[cols*ro+co].b));
      data[ro*cols+co].r = (unsigned char) grey;
      data[ro*cols+co].g = (unsigned char) grey;
      data[ro*cols+co].b = (unsigned char) grey;
    }
  }
  return img;
}

Image* contrast(Image* img, double adj_factor) {
  int rows = (*img).rows;
  int cols = (*img).cols;
  Pixel* data = (*img).data;
  double r;
  double g;
  double b;

  //contrasts image based on adjustment factor
  for(int i = 0; i < rows*cols; i++){
    r = (double)((*img).data[i].r)/255-0.5;
    g =	(double)((*img).data[i].g)/255-0.5;
    b =	(double)((*img).data[i].b)/255-0.5;
    r = r*adj_factor;
    b = b*adj_factor;
    g = g*adj_factor;

    data[i].r = contrast_help(r);
    data[i].g = contrast_help(g);
    data[i].b = contrast_help(b);
  }
  return img;
}

unsigned char contrast_help(double adj_factor){
  //helper function to get an unsigned char for contrast
  unsigned char color;
  if(adj_factor>0.5) {
    color = (unsigned char) 255;
  }
  else if(adj_factor < -0.5) {
    color = (unsigned char) 0;
  }
  else {
    color = (unsigned char) ((adj_factor + 0.5) * 255);
  }
  return color;
}
