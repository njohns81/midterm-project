//header for imageManip
#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

#include "ppmIO.h"
#include <stdlib.h>
#include <stdio.h>

Image* swap(Image* img);

Image* blackout(Image* img);

Image* crop(Image* img, int x1, int y1, int x2, int y2);

Image* grayscale(Image* img);

Image* contrast(Image* img, double adj_factor);

unsigned char contrast_help(double adj_factor);
#endif

